package com.zuitt.s01a1;

public class Activity1 {

    public static void main(String[] args){
        String firstName = "Lawrence";
        String lastName = "Aliñabon";
        double englishGrade = 95;
        double mathGrade = 92.5;
        double scienceGrade = 90.5;
        double averageGrade = (englishGrade + mathGrade + scienceGrade) / 3;

        System.out.println("The average grade of " + firstName +" "+ lastName + " is " + averageGrade);
    }
}
